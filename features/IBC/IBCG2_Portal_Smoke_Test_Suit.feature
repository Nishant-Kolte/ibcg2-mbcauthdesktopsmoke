@IBCG2smoke
Feature: Smoke test script for IBCG2 Portal

  @IBC @Smoke @Login
  Scenario: Verify login to IBCG2 Portal
    Given User is logged in using "IBCUSER" to IBC application
    Then  Verify Home Page is loaded successfully

  @IBC @Smoke @IBCG2_Landing_Pages
  Scenario: Verify IBCG2 Landing Pages
    Then Verify Containers displayed in IBC Home Page
    And Verify Wealth Tab displayed in IBC Home Page
    And Verify Health Tab displayed in IBC Home Page
    And Verify Forms Tab displayed in IBC Home Page
    And Verify TotalRewards Tab displayed in IBC Home Page
    And Verify ResourceCenter Tab displayed in IBC Home Page
    And Verify Administration Tab displayed in IBC Home Page

  @IBC @Smoke @CurrentCoverage
  Scenario:  Verify SSO to H&B application
    Then Verify Health Tab displayed in Home Page
    And Verify Current Coverages Link displayed in Health Menu

  @IBC @Smoke @DBPlan
  Scenario:  Verify SSO to DB application
    Then Verify DB Plan link in Wealth Menu

  @IBC @Smoke @Proxyfunctionality
  Scenario: Verify External Proxy functionality
    When User Clicks on Administration link
    Then Verify that Administration Landing Page loads successfully
    When User Clicks on Employee Search link
    Then Verify that Employee Search Page loads successfully
    When User enter SSN and click on search button
    Then Verify User should be able to see heading You are currently impersonating xxxx user in the header
    When User Clicks on EndImpersonation link in header
    Then Verify User Successfully logout from Impersonation user account

 @IBC @Smoke @LoginManagementLink
  Scenario: Verify Login Management
    When  User Clicks on linkLoginManagement in Home Page
    Then  Verify that Login Management account information page loads successfully
    And   User should be able to see fields "User Name,Password,Email,SecurityQuestions,Answers,Change your User Name,Change your Password,Change your E-mail Address and Change your Security Questions" fields in Account login information page
    When  User Clicks on "Change your Security Questions" in Account Login Information Page
    When  Verify that Modify SQA Page loads successfully
    When  User Change Answer for "Security Question1" field
    And   User Clicks on "Save" button
    And   Verify User should be able to update account login information

   @IBC @Smoke @SiteMapLink
    Scenario:  Verify SiteMap link in header
    When  User Clicks on SiteMapLink in Home Page
    Then  Verify that "G2SiteMap" text is displayed in the URL

   @IBC @Smoke @HelpLink
    Scenario: Verify Help Link in header
    When  User Clicks on Help Link in Header
    Then  Verify that Help Page loads successfully

  @IBC @Smoke @Footer_links
  Scenario: Verify Terms of Use and Privacy Policay links from Footer
    When  User Clicks on Terms of Use link from footer
    Then  Verify that Terms of Use Page Opened in different Window
    When  User Clicks on PrivacyPolicy Link from footer
    Then  Verify that Privacy Policy Page Opened in different window

  @IBC @Smoke @ContactUsLink
  Scenario: Verify Contact Us - Chat and Message Center
    When  User Clicks on ContactUs link in Header
    Then  Verify that Contact Us Page loads successfully

    @IBC @Smoke @logout
    Scenario: Verify user gets logged out successfully
    When  User Clicks on Logout link in header
    Then  Verify User should be able to log out successfully

  @IBC @Smoke @ForgotPasswordflow
  Scenario: Forgot Password Flow
    Given User Navigate to IBC Login Page using "IBCUSER"
    When  User Enter Security Answer1
    And   User Enter Security Answer2
    And   User Enter New Password
    Then  Verify Confirmation Message after saving information
    When  Clicks on Continue button
    Then  Verify that Home Page loads successfully
    When  User Clicks on LoginManagementlink in Home Page
    Then  Verify that Login Management account information page loads successfully
    When  User Clicks on linkChangePassword
    Then  Verify that Change Your Password page loads successfully
    When  Change the Password
    And   Verify User should be able to update login information
    When  User Clicks on Logout link in header
    Then  Verify User should be able to log out successfully