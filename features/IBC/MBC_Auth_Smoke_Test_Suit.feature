@MBCauthsmoke
Feature: Smoke test script for MBCauth

  @Smoke @MBCSmoke @Login
  Scenario: Verify login to MBC
    Given User is logged in using "MBCUSER" to MBC application
    Then  Verify MBC Dashboard Page is loaded successfully

  @Smoke @MBCSmoke @logout
    Scenario: Verify MBC user gets logged out successfully
    When  User Clicks on Logout link on MBC dashboard
    Then  Verify User should be able to log out successfully from MBC
