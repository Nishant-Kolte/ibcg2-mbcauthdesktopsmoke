var action = require('./Keywords.js');
var Objects = require(__dirname + '/../../objects/IBC_locators.js');
var robot = require('robot-js');
var data = require('../../test_data/IBC_TestData.js');
var browser;

module.exports = function () {

    this.Then(/^Navigate to Home Page$/, function (browser) {
                action.isDisplayed("IBCMenu|HomeTab",function() {
                        action.performClick("IBCMenu|HomeTab", function () {
                            browser.timeoutsImplicitWait(data.longwaittime);
                            browser.saveScreenshot('./screenshots/healthlandingpage.png')
                            browser.assert.urlContains("TobsHome");
                        });
                 });

    });
    this.Then(/^Verify that Home Page loads successfully$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.isDisplayed("IBCMenu|HomeTab",function() {
                    browser.assert.urlContains("TobsHome")
                    browser.saveScreenshot('./screenshots/HomePageAfterChangingPassword.png')
                });
            }
        }
    });
    this.Then(/^Verify Home Page is loaded successfully$/, function (browser) {
               browser.pause(data.averagepause);
               browser.timeoutsImplicitWait(data.averagewaittime);
               action.readListData("IBCMenu|HeaderTabs",function(values) {
                   var str = values
                   // console.log(str)
                   // var i,res;
                   // var res = str.split("-");
                   // for(i=0; i < res.length; i++){
                   //     console.log(res[i])
                   //     if(res[i]="Health"){
                   //
                   //     }
                   //
                   // }
                   // browser.url(function (iste_url) {
                   //     console.log(iste_url)
                   // })

                   if(str.includes("Home")) {
                       action.isDisplayed("IBCMenu|HomeTab",function() {
                       });
                   }
                   else if(str.includes("Wealth")) {
                       action.isDisplayed("IBCMenu|WealthTab",function() {
                       });
                   }
                   else if(str.includes("Health")) {
                       action.isDisplayed("IBCMenu|HealthTab",function() {
                       });
                   }

                   if(str.includes("Wealth-Health-Absence")) {
                      console.log("ThreeTobsHome Page displayed")
                   }
                   else if(str.includes("Wealth-Health")) {
                       console.log("TwoTobsHome Page displayed")
                   }
                   else if(str.includes("Health-Absence")) {
                       console.log("TwoTobsHome Page displayed")
                   }
                   else if(str.includes("Wealth-Absence")) {
                       console.log("TwoTobsHome Page displayed")
                   }
                   else if(str.includes("Wealth")) {
                       console.log("WealthLanding Page displayed")
                   }
                   else if(str.includes("Health")) {
                       console.log("HealthLanding Page displayed")
                   }


               });

    });
    this.Then(/^Verify User Successfully logout from Impersonation user account$/, function (browser) {
        browser.timeoutsImplicitWait(data.averagewaittime);
        action.isDisplayed("IBCMenu|HomeTab",function() {
            browser.assert.urlContains("TobsHome")
            browser.saveScreenshot('./screenshots/homepageAfterLoggedoutfromImpersonationaccount.png')
        });
    });
    this.Then(/^Verify that "([^"]*)" text is displayed in the URL$/, function (Expectedtext) {
        this.demoTest = function (browser){
            browser.timeoutsImplicitWait(data.shortwaittime);
            browser.url(function (current_url) {
                var pageurl = current_url.value
                var pageurl = pageurl.toUpperCase();
                if (pageurl.includes("G2SITEMAP")) {
                    console.log("G2SiteMap Page loaded successfully")
                }
                else if(pageurl.includes("ERROR")) {
                    console.log("Error Page loaded for SiteMap Link")
                }
            })
            // browser.assert.urlContains(Expectedtext);
            browser.saveScreenshot('./screenshots/SiteMappage.png')
            browser.back();
        };
    });
    this.Given(/^User should be able to see "([^"]*)" tabs in Home Page$/, function (browser) {
        action.isDisplayed("IBCMenu|WealthTab",function () {
        });
        action.isDisplayed("IBCMenu|HealthTab",function () {
        });
        action.isDisplayed("IBCMenu|AbsenceTab",function () {
        });
    });
    this.Given(/^Also User should be able to see Wealth, Health and Absence containers in Home Page$/, function (browser) {
        action.isDisplayed("IBCMenu|ImagemyWealthContainer",function () {
        });
        action.isDisplayed("IBCMenu|ImagemyHealthContainer",function () {
        });
        action.isDisplayed("IBCMenu|ImagemyAbsenceContaier",function () {
        });
    });
    this.When(/^User Clicks on HealthTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|HealthTab",function () {
        });
    });
    this.When(/^User Clicks on WealthTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|WealthTab",function () {
        });
    });
    this.When(/^User Clicks on TotalRewardsTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|TotalRewardsTab",function () {
        });
    });
    this.When(/^User Clicks on linkCurrentCoverages Link under health page$/, function (browser) {
        action.performClick("IBCHealthPage|linkCurrentCoverages",function () {
        });
    });
    this.When(/^User Clicks on FormsTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|FormsTab",function () {
        });
    });
    this.Given(/^"([^"]*)" tabs should be displayed in Forms Page$/, function (browser) {
        action.isDisplayed("IBCFormsPage|tabWealthForms",function () {
        });
        action.isDisplayed("IBCFormsPage|tabHealthForms",function () {
        });
    });
    this.When(/^User Clicks on ResourceCentertab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|ResourceCenterTab",function () {
        });
    });
    this.When(/^User Clicks on linkLoginManagement in Home Page/, function (browser) {
      //  if(data.TestingEnvironment == "QAI" ) {
            action.performClick("IBCHeaderlinks|linkLoginManagement", function () {
                browser.pause(data.averagepause);
                browser.timeoutsImplicitWait(data.shortwaittime);
                browser.saveScreenshot('./screenshots/LoginAccountManagementpage.png')
            });
       // }
    });
    this.When(/^User Clicks on LoginManagementlink in Home Page/, function (browser) {
                action.performClick("IBCHeaderlinks|linkLoginManagement", function () {
                });
    });

    this.Given(/^User should be able to see fields "([^"]*)" fields in Account login information page$/, function (browser) {
        browser.timeoutsImplicitWait(data.longwaittime);
        action.isDisplayed("IBCSecurityQuestionsPage|labelUsername",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelPassword",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelEmail",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelAnswers",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|linkChangeUsername",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|linkChangeEmailaddress",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|linkChangeSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });
    this.When(/^User Clicks on "([^"]*)" in Account Login Information Page$/, function (browser) {
        action.performClick("IBCSecurityQuestionsPage|linkChangeSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });
    this.When(/^User Change Answer for "([^"]*)" field$/, function (browser) {
           action.getInputBoxText("IBCSecurityQuestionsPage|inputtxtfieldAnswer1", function(txt){
           action.setText("IBCSecurityQuestionsPage|inputtxtfieldAnswer1",txt);
        });
    });
    this.When(/^User Clicks on "([^"]*)" button$/, function (browser) {
        action.performClick("IBCSecurityQuestionsPage|btnSave",function () {
        });
    });
    this.When(/^Verify User should be able to update account login information$/, function (browser) {
       // if(data.TestingEnvironment == "QAI" ) {
       //      action.isDisplayed("IBCSecurityQuestionsPage|confirmationmsg", function () {
       //          browser.saveScreenshot('./screenshots/LoginInformationSaved.png')
       //      });
        browser.timeoutsImplicitWait(data.shortwaittime)
        action.isDisplayed("IBCSecurityQuestionsPage|confirmationmsg",function() {
            browser.saveScreenshot('./screenshots/LoginInformationSaved.png')
        });

        // }
    });
    this.When(/^Verify User should be able to update login information$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime)
        // action.isDisplayed("IBCSecurityQuestionsPage|confirmationmsg", function () {
        //     browser.saveScreenshot('./screenshots/MessageAfterChangingPasswordfromLoginManagement.png')
        // });
        action.isDisplayed("IBCSecurityQuestionsPage|confirmationmsg",function() {
            browser.saveScreenshot('./screenshots/MessageAfterChangingPasswordfromLoginManagement.png')
        });

    });
    this.When(/^User should be able to navigate SiteMap Page and title "SiteMap" should be displayed$/, function (browser) {
        this.demoTest = function (browser) {
            browser.window_handles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
            });
        };
        action.isDisplayed("IBCSiteMapPage|HeadingSiteMap",function () {
        });
        this.demoTest = function (browser){
            browser.closeWindow();
        };
    });
    this.When(/^User Clicks on SiteMapLink in Home Page$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkSiteMap",function () {
            browser.pause(data.shortpause)
        });
    });

    this.When(/^User Clicks on Help Link in Header$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkHelp",function () {
            browser.pause(data.shortpause);
        });
    });
    this.Then(/^Verify that Help Page loads successfully$/, function (browser) {
            browser.window_handles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.assert.urlContains("help")
                browser.saveScreenshot('./screenshots/Helppage.png')
                browser.closeWindow();
                var handle1 = result.value[0];
                browser.switchWindow(handle1);
        });
    });
    this.When(/^User Clicks on ContactUs link in Header$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkContactUsinheader", function () {
                 browser.pause(data.averagepause);
                 browser.timeoutsImplicitWait(data.averagewaittime);
                 browser.saveScreenshot('./screenshots/ContactUspage.png')
            });
        });
    this.When(/^Verify that Contact Us Page loads successfully$/, function (browser) {
            browser.window_handles(function (result) {
                browser.pause(data.averagepause);
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.assert.urlContains("ContactUsHome");
                browser.saveScreenshot('./screenshots/ContactUspage.png')
                // if (data.TestingEnvironment == "QAI" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CSO")
                // {
                    // action.elementDisplayedStatus("IBCContactUsPage|linkChatOnline", function (value) {
                    //     if (value) {
                    //         action.performClick("IBCContactUsPage|linkChatOnline", function () {
                    //             browser.timeoutsImplicitWait(data.shortwaittime);
                    //             browser.assert.urlContains("ChatInquiry");
                    //             browser.assert.urlContains("mchat");
                    //             browser.saveScreenshot('./screenshots/mchatOnlinepage.png')
                    //             browser.back();
                    //             browser.pause(data.averagepause);
                    //         });
                    //     }
                    //     else {
                    //         console.log("ChantOnline link is not Present in Contact Us Page")
                    //     }
                    // })

                    action.readListData("IBCContactUsPage|ChatContainer",function(values) {
                        var str = values
                        if(str.includes("Chat online with a Service Representative")) {
                                    // action.performClick("IBCContactUsPage|linkChatOnline", function () {
                                    //     browser.timeoutsImplicitWait(data.shortwaittime);
                                    //     browser.assert.urlContains("ChatInquiry");
                                    //     browser.assert.urlContains("mchat");
                                    //     browser.saveScreenshot('./screenshots/mchatOnlinepage.png')
                                    //     browser.back();
                                    //     browser.pause(data.averagepause);
                                    // });
                                    action.performClick("IBCContactUsPage|linkChatOnline", function () {
                                        browser.pause(data.shortpause);
                                        browser.timeoutsImplicitWait(data.shortwaittime);
                                        browser.url(function (current_url) {
                                            var pageurl = current_url.value
                                            var pageurl = pageurl.toUpperCase();
                                            if (pageurl.includes("CHATINQUIRY") || pageurl.includes("MCHAT") ) {
                                                console.log("mchat Online Page loaded successfully")
                                            }
                                            else if(pageurl.includes("ERROR")) {
                                                console.log("Error Page loaded for Chat Online link")
                                            }
                                        })
                                        browser.saveScreenshot('./screenshots/mchatOnlinepage.png')
                                        browser.back();
                                        browser.pause(data.averagepause);
                                    });
                        }
                        else {
                            console.log("Chat online with a Service Representative link is not present in Contact Us Page")
                        }
                    });
                // }
                // action.elementDisplayedStatus("IBCContactUsPage|linkVisitMessageCenter", function (value) {
                //     if (value) {
                //         action.performClick("IBCContactUsPage|linkVisitMessageCenter", function () {
                //             browser.pause(data.averagepause);
                //             browser.timeoutsImplicitWait(data.averagewaittime);
                //             browser.assert.urlContains("MessageCenterHome");
                //             browser.saveScreenshot('./screenshots/MessageCenterVisitpage.png')
                //             browser.back();
                //             browser.pause(data.averagepause);
                //         });
                //     }
                //     else {
                //         console.log("VisitMessageCenter link is not Present in Contact Us Page")
                //     }
                // })

                action.readListData("IBCContactUsPage|EmailUsContainer",function(values) {
                    var str = values
                    if(str.includes("Visit the Message Center")) {
                        // action.isDisplayed("IBCContactUsPage|linkVisitMessageCenter", function (value) {
                        //     if (value) {
                                action.performClick("IBCContactUsPage|linkVisitMessageCenter", function () {
                                    browser.pause(data.averagepause);
                                    browser.timeoutsImplicitWait(data.averagewaittime);
                                    browser.url(function (current_url) {
                                        var pageurl = current_url.value
                                        var pageurl = pageurl.toUpperCase();
                                        if (pageurl.includes("MESSAGECENTERHOME")) {
                                            console.log("MessageCenterHome Page loaded successfully")
                                        }
                                        else if(pageurl.includes("ERROR")) {
                                            console.log("Error Page loaded for Visit the Message Center link in Contact Us Page")
                                        }
                                    })
                                    browser.assert.urlContains("MessageCenterHome");
                                    browser.saveScreenshot('./screenshots/MessageCenterVisitpage.png')
                                    browser.back();
                                    browser.pause(data.averagepause);
                                });
                            // }
                        // })
                    }
                    else {
                        console.log("VisitMessageCenter link is not Present in Contact Us Page")
                    }
                });

                browser.closeWindow();
                var handle1 = result.value[0];
                browser.switchWindow(handle1);
            });
            browser.back();
            browser.pause(data.averagepause);
    });
    this.When(/^User Clicks on PrivacyPolicy Link from footer$/, function (browser) {
        action.performClick("IBCLoginPage|linkPrivacyPolicy",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });
    this.When(/^Verify that Privacy Policy Page Opened in different window$/, function (browser) {
           browser.timeoutsImplicitWait(data.shortwaittime);
            browser.window_handles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.assert.urlContains("G2PrivacyPolicy")
                browser.saveScreenshot('./screenshots/PrivacyPolicyWindow.png')
                browser.closeWindow();
                var handle1 = result.value[0];
                browser.switchWindow(handle1);
        });
    });
    this.When(/^User Clicks on Terms of Use link from footer$/, function (browser) {
        action.performClick("IBCLoginPage|linkTermsofUse",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });
    this.Then(/^Verify that Terms of Use Page Opened in different Window$/, function (browser) {
            browser.timeoutsImplicitWait(data.shortwaittime);
            browser.window_handles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.assert.urlContains("G2TermsOfUse")
                browser.saveScreenshot('./screenshots/TermsOfUsepage.png')
                browser.closeWindow();
                var handle1 = result.value[0];
                browser.switchWindow(handle1);
        });
      });
    this.Then(/^Verify that Wealth Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.assert.urlContains("WealthTOBOutage")
    });
    this.Then(/^Health Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.assert.urlContains("HealthLanding")
    });
    this.Then(/^Current Coverage Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.assert.urlContains("hb-")
    });
    this.Then(/^Verify that Total Rewards Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.assert.urlContains("OnlineTotalRewardsHome")
    });
    this.Then(/^Verify that Forms Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.assert.urlContains("G2Forms")
    });
    this.Then(/^Verify that Resource Center Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.assert.urlContains("ResourceCenterHome")
    });
    this.Then(/^Verify that Login Management account information page loads successfully$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                browser.timeoutsImplicitWait(data.shortwaittime);
                browser.assert.urlContains("LoginManagementOverview")
            }
        }
    });
    this.Then(/^Verify that Modify SQA Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.assert.urlContains("ModifySQA")
    });
    this.When(/^User Clicks on Logout link in header$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkLogOut",function () {
         browser.pause(data.longpause);
            browser
                .acceptAlert()
        });
    });
    this.When(/^User Clicks on EndImpersonation link in header$/, function (browser) {
        action.performClick("IBCAdminLandingPage|linkEndImpersonation",function () {
            browser.pause(data.longpause);
            browser
                .acceptAlert()
        });
    });

    this.When(/^Verify$/, function (browser) {
        action.performClick("IBCAdminLandingPage|linkEndImpersonation",function () {
            browser.pause(data.longpause);
            browser
                .acceptAlert()
        });
    });

    this.Then(/^Verify User should be able to log out successfully$/, function (browser) {
        // action.isDisplayed("IBCLoginPage|LogoutMessage",function () {
        //     browser.saveScreenshot('./screenshots/LogoutMessage.png')
        // });
        action.isDisplayed("IBCLoginPage|LogoutMessage",function() {
            browser.saveScreenshot('./screenshots/LogoutMessage.png')
        });
    });
    this.When(/^User Enter Security Answer1$/, function (browser) {
                browser.pause(data.averagepause);
                browser.timeoutsImplicitWait(data.averagewaittime);
                action.getText("IBCForgotPasswordYourSecurityQuestionsPage|inputlabelsecuriyquestion1", function (txt) {
                    var replacedstring = txt.replace("?", "");
                    var res = replacedstring.split(" ");
                    var lastarrayvalue = res[res.length - 1];
                    action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputSecurityAnswer1", lastarrayvalue);
                    action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnSQANext", function () {
                        browser.timeoutsImplicitWait(data.averagewaittime);
                    });
                })
    });
    this.When(/^User Enter Security Answer2$/, function (browser) {
                browser.pause(data.averagepause);
                browser.timeoutsImplicitWait(data.shortwaittime);
                action.getText("IBCForgotPasswordYourSecurityQuestionsPage|inputlabelsecuriyquestion1", function (txt) {
                    var replacedstring = txt.replace("?", "");
                    var res = replacedstring.split(" ");
                    var lastarrayvalue = res[res.length - 1];
                    action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputSecurityAnswer1", lastarrayvalue);
                    action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnSQANext", function () {
                        browser.timeoutsImplicitWait(data.averagewaittime);
                    });
                })
    });
    this.When(/^User Enter New Password$/, function (browser) {
                browser.pause(data.averagepause);
                browser.timeoutsImplicitWait(data.shortwaittime);
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputnewpassword1", data.NewPassword);
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputreenternewpassword1", data.NewPassword);
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnContainerIDNext", function () {
                    browser.timeoutsImplicitWait(data.averagewaittime);
                });
                browser.saveScreenshot('./screenshots/ReviewandSaveInformationPage.png')
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnFinishNext", function () {
                    browser.timeoutsImplicitWait(data.averagewaittime);
                });
    });
    this.Then(/^Verify Confirmation Message after saving information$/, function (browser) {
                browser.pause(data.averagepause);
                action.elementDisplayedStatus("IBCForgotPasswordYourSecurityQuestionsPage|TitleInformationsaved", function () {
                    browser.saveScreenshot('./screenshots/ConfirmationMessageAfterChangingPassword.png')
                });
    });
    this.When(/^Clicks on Continue button$/, function (browser) {
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnContinue", function () {
                    browser.pause(data.shortpause);
                });
    });
    this.When(/^User Clicks on linkChangePassword$/, function (browser) {
                action.performClick("IBCLoginManagementPage|linkChangePassword", function () {
                    browser.pause(data.shortpause);

                });
    });
    this.Then(/^Verify that Change Your Password page loads successfully$/, function (browser) {
                browser.timeoutsImplicitWait(data.shortwaittime);
                browser.assert.urlContains("ModifyPassword")
    });
    this.When(/^Change the Password$/, function (browser) {
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputOldPassword", data.NewPassword);
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputNewPassword", data.DefaultPassword);
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputReNewPassword", data.DefaultPassword);
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnSaveModifyPassword", function () {
                    browser.timeoutsImplicitWait(data.averagewaittime);
                });
    });
    this.Given(/^Verify Tabs displayed in IBC Home Page$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        action.isDisplayed("IBCMenu|WealthTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|WealthTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("Wealth")
                    console.log("Wealth page is loaded successfully");
                });
            }
        })
        action.isDisplayed("IBCMenu|HealthTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|HealthTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("Health")
                    console.log("Health page is loaded successfully");
                    // action.performClick("IBCHealthPage|linkCurrentCoverages", function () {
                    //     browser.timeoutsImplicitWait(data.shortwaittime);TotalRewardsTab
                    //     // browser.assert.urlContains("hb-")
                    //     browser.assert.urlContains("CurrentCoverage")
                    // });
                });
            }
        })
        //       action.isDisplayed("IBCMenu|AbsenceTab",function(value){
        // if (value) {
        // action.performClick("IBCMenu|AbsenceTab", function () {
        //     browser.pause(data.averagepause);
        //     browser.assert.urlContains("DocProdCommunicationHistory");
        // });
        //  }
        // })
        var n = data.urlQAI.includes("PBSPHL")
        if (n && data.TestingEnvironment == "QAI") {
             console.log("Total Rewards tab is not found in Headerlist");
        }
        else {
            action.performClick("IBCMenu|", function () {
                browser.timeoutsImplicitWait(data.shortwaittime);
                browser.assert.urlContains("OnlineTotalRewardsHome")
                console.log("Total Rewards page is loaded successfully");
            });
        }

        action.isDisplayed("IBCMenu|FormsTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|FormsTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("G2Forms")
                    console.log("Forms page is loaded successfully");
                });
            }
        })
        action.isDisplayed("IBCMenu|ResourceCenterTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|ResourceCenterTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("ResourceCenterHome.")
                    console.log("Resource Center Page is loaded successfully");
                });
            }
        })

        var n = data.urlQAI.includes("PBSPHL")
        if (n && data.TestingEnvironment == "QAI") {
            action.isDisplayed("IBCMenu|AdministrationTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|AdministrationTab", function () {
                        browser.timeoutsImplicitWait(data.shortwaittime);
                        browser.assert.urlContains("G2AdminLandingPage")
                    });
                }
            })
        }
        if (data.TestingEnvironment == "PROD" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CSO" || data.TestingEnvironment == "CIDEV") {
            action.isDisplayed("IBCMenu|AdministrationTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|AdministrationTab", function () {
                        browser.pause(data.averagepause);
                        browser.assert.urlContains("G2AdminLandingPage.")
                    });
                }
            })
        }
        action.isDisplayed("IBCMenu|HomeTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|HomeTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);

                    browser.assert.urlContains("TobsHome")
                });
            }
        })

    });
    this.Then(/^Containers displayed in Home Page$/, function (browser) {
        action.performClick("IBCMenu|HomeTab",function () {
            browser.pause(data.shortpause);
        });
        action.isDisplayed("IBCMenu|ImagemyWealthContainer",function (value) {
            if (value) {
            }
        })
        action.isDisplayed("IBCMenu|ImagemyHealthContainer",function (value) {
            if (value) {
            }
        })
        // action.isDisplayed("IBCMenu|ImagemyAbsenceContaier",function (value) {
        //     if (value) {
        //     }
        // })
    });
    this.Given(/^User Clicks on Administration link$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Administration")) {
                action.isDisplayed("IBCMenu|AdministrationTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|AdministrationTab", function () {
                            browser.pause(data.shortpause);
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            // browser.assert.urlContains("G2AdminLandingPage")
                        });
                    }
                    else  {
                        console.log("Administration Tab is not present in Header")
                    }
                })
            }
            else  {
                console.log("Administration Tab is not present in Header")
            }
        });
    });
    this.Then(/^Verify that Admin Landing Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        browser.url(function (current_url) {
            var pageurl = current_url.value
            var pageurl = pageurl.toUpperCase();
            if (pageurl.includes("G2ADMINLANDINGPAGE")) {
                console.log("Admin Landing Page loads successfully")
            }
            else if(pageurl.includes("ERROR")) {
                console.log("Error Page loaded for Administration Tab")
            }
        })
        browser.saveScreenshot('./screenshots/Administrationpage.png')
        browser.back();
        browser.pause(data.shortpause);
        // browser.assert.urlContains("G2AdminLandingPage")
    });
    this.Then(/^Verify that Administration Landing Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        browser.url(function (current_url) {
            var pageurl = current_url.value
            var pageurl = pageurl.toUpperCase();
            if (pageurl.includes("G2ADMINLANDINGPAGE")) {
                console.log("Admin Landing Page loads successfully")
            }
            else if(pageurl.includes("ERROR")) {
                console.log("Error Page loaded for Administration Tab")
            }
        })
        browser.saveScreenshot('./screenshots/Administrationpage.png')
    });
    this.Given(/^User Clicks on Employee Search link$/, function (browser) {
            action.isDisplayed("IBCAdminLandingPage|linkEmployeesearch", function (value) {
                if (value) {
                    action.performClick("IBCAdminLandingPage|linkEmployeesearch", function () {
                        browser.pause(data.averagepause);
                        browser.assert.urlContains("G2EmployeeSearch")
                        browser.saveScreenshot('./screenshots/EmployeeSearchpage.png')
                    });
                }
            })
    });
    this.Then(/^Verify that Employee Search Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        browser.assert.urlContains("G2EmployeeSearch")
    });
    this.When(/^User enter SSN and click on search button$/, function (browser) {
        // Search by SSN
        action.performClick("IBCAdminLandingPage|SearchBySSN", function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
        if(data.TestingEnvironment == "QAI") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyQAI);
        }
        else if(data.TestingEnvironment == "CIDEV") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyCIDEV);
        }
        else if(data.TestingEnvironment == "CITEST") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyCITEST);
        }
        else if(data.TestingEnvironment == "CSO") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyCSO);
        }
        else if(data.TestingEnvironment == "PROD") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyPROD);
        }

        action.performClick("IBCAdminLandingPage|btnSearchProxy", function () {
            browser.timeoutsImplicitWait(data.averagewaittime);
        });
        browser.pause(data.averagepause);
        browser.assert.urlContains("G2EmployeeSearch")
        browser.saveScreenshot('./screenshots/ResultsAfterSearchBySSN.png')
        // Keyword field should be empty after SSN Search
        action.getText("IBCAdminLandingPage|inputKeyword", function (txt) {
            var n = txt.includes("");
            if(n){
                console.log("Keyword field is empty after SSN Search");
            }
        })
        // Search by Lastname
        action.performClick("IBCAdminLandingPage|SearchByLastName", function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
        // Keyword field should not be empty after Lastname Search
        action.getText("IBCAdminLandingPage|inputKeyword", function (txt) {
            if(txt != ""){
                console.log("Keyword field should not be empty after Lastname Search");
            }
        })
        browser.saveScreenshot('./screenshots/ResultsAfterSearchByLastName.png')
        action.getText("IBCAdminLandingPage|SSNfirstrowresult", function (txt) {
            var n = txt.includes("*****");
            if(n){
                console.log("Five digit of the SSN Number is masked");
            }
        })
        action.getText("IBCAdminLandingPage|LastNamefirstrowresult", function (txt) {
            var res = txt.trim();
            action.setText("IBCAdminLandingPage|inputKeyword", res);
            action.performClick("IBCAdminLandingPage|btnSearchProxy", function () {
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
            browser.pause(data.averagepause);
            browser.assert.urlContains("G2EmployeeSearch")
        })
        // Search by SSN
        action.performClick("IBCAdminLandingPage|SearchBySSN", function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });

        if(data.TestingEnvironment == "QAI") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyQAI);
        }
        else if(data.TestingEnvironment == "CIDEV") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyCIDEV);
        }
        else if(data.TestingEnvironment == "CITEST") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyCITEST);
        }
        else if(data.TestingEnvironment == "CSO") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyCSO);
        }
        else if(data.TestingEnvironment == "PROD") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyPROD);
        }

        action.performClick("IBCAdminLandingPage|btnSearchProxy", function () {
            browser.timeoutsImplicitWait(data.averagewaittime);
        });
        browser.pause(data.averagepause);
        browser.assert.urlContains("G2EmployeeSearch")

        action.getText("IBCAdminLandingPage|LastNamefirstrowresult", function (txt) {
            var Lastnametext = txt.trim();
            action.getText("IBCAdminLandingPage|FirstNamefirstrowresult", function (txt) {
                var Firstnametext = txt.trim();
                action.performClick("IBCAdminLandingPage|selectlink", function () {
                    browser.pause(data.averagepause)
                    browser.timeoutsImplicitWait(data.averagewaittime);
                });
                action.getText("IBCAdminLandingPage|labelImpersonate", function (value) {
                    if (value.includes(Lastnametext) && value.includes(Firstnametext) ) {
                        browser.saveScreenshot('./screenshots/EndImpersonationPage.png')
                    }
                })
            });
        });
    });
    this.Then(/^Verify that user should be able to see results$/, function (browser) {
        browser.pause(data.averagepause);
        browser.assert.urlContains("G2EmployeeSearch")
    });
    this.When(/^User clicks on Select link$/, function (browser) {
            action.isDisplayed("IBCAdminLandingPage|selectlink", function (value) {
                if (value) {
                    action.performClick("IBCAdminLandingPage|selectlink", function () {
                        browser.pause(data.shortpause)
                        browser.timeoutsImplicitWait(data.averagewaittime);
                        // browser.assert.urlContains("WelcomePage")
                    });
                }
            })
    });
    this.Then(/^Verify User should be able to see heading You are currently impersonating xxxx user in the header$/, function (browser) {
        action.isDisplayed("IBCAdminLandingPage|labelImpersonate", function (value) {
            if (value) {
                browser.saveScreenshot('./screenshots/EndImpersonationPage.png')
            }
        })
    });
    this.Given(/^Verify DC Plan link in Wealth Menu$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Wealth")) {
                action.isDisplayed("IBCMenu|WealthTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|WealthTab", function () {
                            browser.timeoutsImplicitWait(data.longwaittime);
                            // browser.assert.urlContains("Wealth")
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("WEALTHLANDING")) {
                                    console.log("Wealth Landing Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for Wealth Tab")
                                }
                            })
                            action.readListData("IBCMenu|PlansLink",function(values) {
                                var str = values
                                if(str.includes("Plans")) {
                                    // action.isDisplayed("IBCMenu|Plan401", function (value) {
                                    //     if (value) {
                                    //         var objLocation = Objects.sections.IBCMenu.elements.PlansDropdown.selector;
                                    //         var locateStrategy = Objects.sections.IBCMenu.elements.PlansDropdown.locateStrategy;
                                    //         browser.useXpath().moveToElement(objLocation, 3, 3);
                                    //         action.performClick("IBCMenu|Plan401", function () {
                                    //             browser.pause(data.averagepause)
                                    //             browser.timeoutsImplicitWait(data.longwaittime);
                                    //             browser.saveScreenshot('./screenshots/DCPlanPage.png')
                                    //             browser.assert.urlContains("dcaccounts")
                                    //             browser.back();
                                    //             browser.timeoutsImplicitWait(data.shortwaittime);
                                    //         });
                                    //     }
                                    //     else {
                                    //         console.log("DC Plan Link is not present in Wealth Menu")
                                    //     }
                                    // })
                                    var xpath="//span[contains(text(),'Plans')]";
                                    var xpath1="//span[contains(text(),'Plans')]/../../ul/li/a//span[contains(text(),'"+data.DCPlan+"')]";
                                    var xpath2="//span[contains(text(),'Plans')]/../../ul/li/div/div/a//span[contains(text(),'"+data.DCPlan+"')]";

                                    browser.useXpath().moveToElement(xpath, 0, 1).pause(3000);
                                    action.readListData("IBCMenu|Plans",function(values) {
                                        var str = values
                                        if(str.includes(data.DCPlan)) {
                                            action.getWebElementsCount("IBCMenu|Plans", function (count) {
                                               if(count>1) {
                                                   browser.useXpath().getText("//span[contains(text(),'Plans')]/../../ul/li/a//span[contains(text(),'" + data.DCPlan + "')]", function (result) {
                                                       if (result.value.includes(data.DCPlan)) {
                                                           browser.click(xpath1);
                                                           // browser.pause(5000);
                                                           // browser.pause(data.averagepause)
                                                           browser.timeoutsImplicitWait(data.longwaittime);
                                                           browser.saveScreenshot('./screenshots/DCPlanPage.png')
                                                           browser.assert.urlContains("dcaccounts")
                                                           browser.back();
                                                           browser.timeoutsImplicitWait(data.shortwaittime);
                                                       }
                                                   });
                                               }
                                               else {
                                                   browser.useXpath().getText("//span[contains(text(),'Plans')]/../../ul/li/div/div/a//span[contains(text(),'"+data.DCPlan+"')]", function (result) {
                                                       if (result.value.includes(data.DCPlan)) {
                                                           browser.click(xpath2);
                                                           browser.pause(5000);
                                                           browser.pause(data.averagepause)
                                                           browser.timeoutsImplicitWait(data.longwaittime);
                                                           browser.saveScreenshot('./screenshots/DCPlanPage.png')
                                                           browser.assert.urlContains("dcaccounts")
                                                           browser.back();
                                                           browser.timeoutsImplicitWait(data.shortwaittime);
                                                       }
                                                   });
                                               }
                                            });
                                        }
                                        else {
                                           console.log("DC Plan is not displayed in Plans dropdown")
                                        }
                                    });
                                }
                                else {
                                    console.log("Plans link is not present in the Wealth Menu")
                                }
                            });
                        });
                    }
                    else {
                        console.log("Wealth Tab is not present in header")
                    }
                });
            }
            else {
                console.log("Wealth Tab is not present in the Header")
            }
        });
    });
    this.Given(/^Verify DB Plan link in Wealth Menu$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Wealth")) {
                action.isDisplayed("IBCMenu|WealthTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|WealthTab", function () {
                            browser.timeoutsImplicitWait(data.longwaittime);
                            // browser.assert.urlContains("Wealth")
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("WEALTHLANDING")) {
                                    console.log("Wealth Landing Page loads successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for Wealth Tab")
                                }
                            })
                            action.readListData("IBCMenu|PlansLink",function(values) {
                                var str = values
                                if(str.includes("Plans")) {
                                    // action.isDisplayed("IBCMenu|PlanMCNNonRepresented", function (value) {
                                    //     if (value) {
                                    //         var objLocation = Objects.sections.IBCMenu.elements.PlansDropdown.selector;
                                    //         var locateStrategy = Objects.sections.IBCMenu.elements.PlansDropdown.locateStrategy;
                                    //         browser.useXpath().moveToElement(objLocation, 3, 3);
                                    //         browser.pause(data.shortpause);
                                    //         action.performClick("IBCMenu|PlanMCNNonRepresented", function () {
                                    //             browser.pause(data.longpause);
                                    //             browser.timeoutsImplicitWait(data.averagewaittime);
                                    //             browser.saveScreenshot('./screenshots/DBPlanPage.png')
                                    //             browser.assert.urlContains("pm4")
                                    //             browser.back();
                                    //             browser.pause(data.averagepause);
                                    //         });
                                    //     }
                                    //     else {
                                    //         console.log("DB Plan Link is not present in Wealth Menu")
                                    //     }
                                    // });

                                    var xpath="//span[contains(text(),'Plans')]";
                                    var xpath1="//span[contains(text(),'Plans')]/../../ul/li/a//span[contains(text(),'"+data.DBPlan+"')]";
                                    var xpath2="//span[contains(text(),'Plans')]/../../ul/li/div/div/a//span[contains(text(),'"+data.DBPlan+"')]";
                                    browser.useXpath().moveToElement(xpath, 0, 1).pause(3000);
                                    action.readListData("IBCMenu|Plans",function(values) {
                                        var str = values
                                        if(str.includes(data.DBPlan)) {
                                            action.getWebElementsCount("IBCMenu|Plans", function (count) {
                                                if(count>1) {
                                                    browser.useXpath().getText("//span[contains(text(),'Plans')]/../../ul/li/a//span[contains(text(),'"+data.DBPlan+"')]", function (result) {
                                                        if (result.value.includes(data.DBPlan)) {
                                                            browser.click(xpath1);
                                                            browser.pause(5000);
                                                            browser.pause(data.averagepause)
                                                            browser.timeoutsImplicitWait(data.longwaittime);
                                                            browser.saveScreenshot('./screenshots/DBPlanPage.png')
                                                            browser.assert.urlContains("pm4")
                                                            browser.back();
                                                            browser.timeoutsImplicitWait(data.shortwaittime);
                                                        }
                                                    });
                                                }
                                                else {
                                                    browser.useXpath().getText("//span[contains(text(),'Plans')]/../../ul/li/div/div/a//span[contains(text(),'"+data.DBPlan+"')]", function (result) {
                                                        if (result.value.includes(data.DBPlan)) {
                                                            browser.click(xpath2);
                                                            browser.pause(5000);
                                                            browser.pause(data.averagepause)
                                                            browser.timeoutsImplicitWait(data.longwaittime);
                                                            browser.saveScreenshot('./screenshots/DBPlanPage.png')
                                                            browser.assert.urlContains("pm4")
                                                            browser.back();
                                                            browser.timeoutsImplicitWait(data.shortwaittime);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                        else {
                                            console.log("DB Plan is not displayed in Plans dropdown")
                                        }
                                    });
                                }
                                else {
                                    console.log("Plans link is not present in the Wealth Menu")
                                }
                            });
                        });
                    }
                    else {
                        console.log("Wealth Tab is not present Header")
                    }
                });
            }
            else{
                console.log("Wealth Tab is not present in header")
            }
        });
    });

    this.Given(/^Verify Tabs displayed in IBC Home Page$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        action.isDisplayed("IBCMenu|WealthTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|WealthTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("Wealth")
                    console.log("Wealth page is loaded successfully");
                });
            }
        })
        action.isDisplayed("IBCMenu|HealthTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|HealthTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("Health")
                    console.log("Health page is loaded successfully");
                    // action.performClick("IBCHealthPage|linkCurrentCoverages", function () {
                    //     browser.timeoutsImplicitWait(data.shortwaittime);TotalRewardsTab
                    //     // browser.assert.urlContains("hb-")
                    //     browser.assert.urlContains("CurrentCoverage")
                    // });
                });
            }
        })
        //       action.isDisplayed("IBCMenu|AbsenceTab",function(value){
        // if (value) {
        // action.performClick("IBCMenu|AbsenceTab", function () {
        //     browser.pause(data.averagepause);
        //     browser.assert.urlContains("DocProdCommunicationHistory");
        //     console.log("DocProdCommunicationHistory page is displayed");
        // });
        //  }
        // })
        var n = data.urlQAI.includes("PBSPHL")
        if (n && data.TestingEnvironment == "QAI") {
            console.log("Total Rewards tab is not found in Headerlist");
        }
        else {
            action.performClick("IBCMenu|TotalRewardsTab", function () {
                browser.timeoutsImplicitWait(data.shortwaittime);
                browser.assert.urlContains("OnlineTotalRewardsHome")
                console.log("Total Rewards page is loaded successfully");
            });
        }

        action.isDisplayed("IBCMenu|FormsTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|FormsTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("G2Forms")
                    console.log("Forms page is loaded successfully");
                });
            }
        })
        action.isDisplayed("IBCMenu|ResourceCenterTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|ResourceCenterTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("ResourceCenterHome.")
                    console.log("Resource Center Page is loaded successfully");
                });
            }
        })

        var n = data.urlQAI.includes("PBSPHL")
        if (n && data.TestingEnvironment == "QAI") {
            action.isDisplayed("IBCMenu|AdministrationTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|AdministrationTab", function () {
                        browser.timeoutsImplicitWait(data.shortwaittime);
                        browser.assert.urlContains("G2AdminLandingPage")
                        console.log("Administration Page is loaded successfully");
                    });
                }
            })
        }
        if (data.TestingEnvironment == "PROD" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CSO" || data.TestingEnvironment == "CIDEV") {
            action.isDisplayed("IBCMenu|AdministrationTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|AdministrationTab", function () {
                        browser.pause(data.averagepause);
                        browser.assert.urlContains("G2AdminLandingPage.")
                    });
                }
            })
        }
        action.isDisplayed("IBCMenu|HomeTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|HomeTab", function () {
                    browser.timeoutsImplicitWait(data.shortwaittime);
                    browser.assert.urlContains("TobsHome")
                });
            }
        })

    });


    this.Then(/^Verify WealthContainer displayed in IBC Home Page$/, function (browser) {

        action.isDisplayed("IBCMenu|ImagemyWealthContainer",function (value) {
            if (value) {
                console.log("Wealth Container displayed in Home Page")
            }
            else {
                console.log("Wealth Container is not displayed in Home Page")
            }

        })
     });
    this.Then(/^Verify HealthContainer displayed in IBC Home Page$/, function (browser) {

        action.isDisplayed("IBCMenu|ImagemyHealthContainer",function (value) {
            if (value) {
                console.log("Health Container displayed in Home Page")
            }
            else {
                console.log("Health Container is not displayed in Home Page")
            }
        })
    });

    this.Then(/^Verify Health Tab displayed in IBC Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Health")) {
                action.isDisplayed("IBCMenu|HealthTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|HealthTab", function () {
                            browser.pause(data.longpause);
                            browser.timeoutsImplicitWait(data.longwaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("HEALTHLANDING")) {
                                    console.log("Health Landing Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")){
                                    console.log("Error Page loaded for Health Tab")
                                }
                            })
                            browser.saveScreenshot('./screenshots/healthlandingpage.png')
                            browser.back();
                            browser.pause(data.shortpause);
                        });
                    }
                    else{
                        console.log("Health Tab is not present in header")
                        browser.back();
                        browser.pause(data.averagepause);
                    }
                })
            }
            else{
                console.log("Health Tab is not present in header")
            }
        });
    });

    this.Then(/^Verify Health Tab displayed in Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Health")) {
                action.isDisplayed("IBCMenu|HealthTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|HealthTab", function () {
                            browser.pause(data.longpause);
                            browser.timeoutsImplicitWait(data.longwaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("HEALTHLANDING")) {
                                    console.log("Health Landing Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")){
                                    console.log("Error Page loaded for Health Tab")
                                }
                            })
                            browser.saveScreenshot('./screenshots/healthlandingpage.png')
                        });
                    }
                    else{
                        console.log("Health Tab is not present in header")
                        browser.back();
                        browser.pause(data.averagepause);
                    }
                })
            }
            else{
                console.log("Health Tab is not present in header")
            }
        });
    });

    // this.Then(/^Verify Home Tab displayed in IBC Home Page$/, function (browser) {
    //     action.isDisplayed("IBCMenu|HomeTab", function (value) {
    //         if (value) {
    //             action.performClick("IBCMenu|HomeTab", function () {
    //                 browser.timeoutsImplicitWait(data.shortwaittime);
    //                 browser.assert.urlContains("Home")
    //             });
    //         }
    //         else {
    //             console.log("Home Tab is not present in Header")
    //         }
    //     })
    // });
    this.Then(/^Verify Wealth Tab displayed in IBC Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Wealth")) {
                action.isDisplayed("IBCMenu|WealthTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|WealthTab", function () {
                            browser.pause(data.shortpause);
                            browser.timeoutsImplicitWait(data.longwaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("WEALTHLANDING")) {
                                    console.log("Wealth Landing Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for Wealth Tab")
                                }
                            })
                            // browser.assert.urlContains("Wealth")
                            browser.saveScreenshot('./screenshots/wealthlandingpage.png')
                            browser.back();
                            browser.pause(data.shortpause);
                        });
                    }
                    else {
                        console.log("Wealth Tab is not present in Header")
                        browser.back();
                        browser.pause(data.shortpause);
                    }
                })
            }
            else {
                console.log("Wealth Tab is not present in the Header")
            }
        });
    });

    this.Then(/^Verify Containers displayed in IBC Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Home")) {
                if(str.includes("Wealth")) {
                    action.isDisplayed("IBCMenu|ImagemyWealthContainer", function (value) {
                        if (value) {
                            console.log("Wealth Container displayed in Home Page")
                        }
                        else {
                            console.log("Wealth Container is not displayed in Home Page")
                        }

                    })
                }
                if(str.includes("Health")) {
                    action.isDisplayed("IBCMenu|ImagemyHealthContainer", function (value) {
                        if (value) {
                            console.log("Health Container displayed in Home Page")
                        }
                        else {
                            console.log("Health Container is not displayed in Home Page")
                        }
                    })
                }
            }
            else {
                console.log("Home Tab is not present in the Header")
            }
        });
    });



    this.Then(/^Verify TotalRewards Tab displayed in IBC Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Total Rewards")) {
                action.isDisplayed("IBCMenu|TotalRewardsTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|TotalRewardsTab", function () {
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("ONLINETOTALREWARDSHOME")) {
                                    console.log("OnlineTotalRewards Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for TotalRewards Tab")
                                }
                            })
                            // browser.assert.urlContains("OnlineTotalRewardsHome")
                            browser.saveScreenshot('./screenshots/OnlineTotalRewardspage.png')
                            browser.back();
                            browser.pause(data.shortpause);
                        });
                    }
                    else {
                        console.log("OnlineTotal Rewards Tab is not present in Header")
                        browser.back();
                        browser.pause(data.shortpause);
                    }
                })
            }
            else {
                console.log("OnlineTotal Rewards Tab is not present in Header")
            }
        });
    });
    this.Then(/^Verify Forms Tab displayed in IBC Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Forms")) {
                action.isDisplayed("IBCMenu|FormsTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|FormsTab", function () {
                            browser.pause(data.shortpause);
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("G2FORMS")) {
                                    console.log("G2Forms Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for Forms Tab")
                                }
                            })
                            // browser.assert.urlContains("G2Forms")
                            browser.saveScreenshot('./screenshots/G2Formspage.png')
                        });
                    }
                    else {
                        console.log("Forms Tab is not present in Header")
                    }
                })
            }
            else {
                console.log("Forms Tab is not present in Header")
            }
        });
    });
    this.Then(/^Verify ResourceCenter Tab displayed in IBC Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Resource Center")) {
                action.isDisplayed("IBCMenu|ResourceCenterTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|ResourceCenterTab", function () {
                            browser.pause(data.shortpause);
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("RESOURCECENTERHOME")) {
                                    console.log("ResourceCenter Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for ResourceCenter Tab")
                                }
                            })
                            // browser.assert.urlContains("ResourceCenterHome")
                            browser.saveScreenshot('./screenshots/ResourceCenterHomepage.png')
                            browser.back();
                            browser.pause(data.shortpause);
                        });
                    }
                    else {
                        console.log("ResourceCenter Tab is not present in Menu")
                        browser.back();
                        browser.pause(data.shortpause);
                    }
                })
            }
            else {
                console.log("ResourceCenter Tab is not present in Menu")
            }
        });
    });
    this.Then(/^Verify Administration Tab displayed in IBC Home Page$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Administration")) {
                action.isDisplayed("IBCMenu|AdministrationTab", function (value) {
                    if (value) {
                        action.performClick("IBCMenu|AdministrationTab", function () {
                            browser.pause(data.shortpause);
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                if (pageurl.includes("G2ADMINLANDINGPAGE")) {
                                    console.log("Admin Landing Page loaded successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for Administration Tab")
                                }
                            })
                            // browser.assert.urlContains("G2AdminLandingPage")
                            browser.saveScreenshot('./screenshots/Administrationpage.png')
                            browser.back();
                            browser.pause(data.shortpause);
                        });
                    }
                    else  {
                        console.log("Administration Tab is not present in Header")
                    }
                })
            }
            else  {
                console.log("Administration Tab is not present in Header")
            }
        });
    });
    this.Then(/^Verify Current Coverages Link displayed in Health Menu$/, function (browser) {
        action.readListData("IBCMenu|HeaderTabs",function(values) {
            var str = values
            if(str.includes("Health")) {
                // action.isDisplayed("IBCHealthPage|linkCurrentCoverages", function (value) {
                //     if (value) {
                //         action.performClick("IBCHealthPage|linkCurrentCoverages", function () {
                //             browser.pause(data.longpause);
                //             browser.timeoutsImplicitWait(data.averagewaittime);
                //             browser.url(function (current_url) {
                //                 var pageurl = current_url.value
                //                 var pageurl = pageurl.toUpperCase();
                //                 if (pageurl.includes("COVERAGE") || pageurl.includes("HB-") || pageurl.includes("MYHEALTH")) {
                //                     console.log("Current Coverage Page loads successfully")
                //                 }
                //                 else if(pageurl.includes("ERROR")) {
                //                     console.log("Error Page loaded for CurrentCoverage Link")
                //                 }
                //             })
                //             browser.saveScreenshot('./screenshots/currentcoveragepage.png')
                //             // browser.assert.urlContains("hb-")
                //             // if ( browser.assert.urlContains("Coverage") || browser.assert.urlContains("hb-") || browser.assert.urlContains("myhealth") )
                //             // {
                //             //     browser.back();
                //             //     browser.timeoutsImplicitWait(data.shortwaittime);
                //             // }
                //         });
                //     }
                //     else {
                //         console.log("Current Coverage Link is not present in Health Menu")
                //         browser.back();
                //         browser.pause(data.averagepause);
                //     }
                // })

                action.readListData("IBCMenu|PlansLink",function(values) {
                    var str = values
                    if(str.includes("Current Coverages")) {
                        action.performClick("IBCHealthPage|linkCurrentCoverages", function () {
                            browser.pause(data.longpause);
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            browser.url(function (current_url) {
                                var pageurl = current_url.value
                                var pageurl = pageurl.toUpperCase();
                                browser.saveScreenshot('./screenshots/currentcoveragepage.png')
                                if (pageurl.includes("COVERAGE") || pageurl.includes("HB-") || pageurl.includes("MYHEALTH")) {
                                    console.log("Current Coverage Page loads successfully")
                                }
                                else if(pageurl.includes("ERROR")) {
                                    console.log("Error Page loaded for CurrentCoverage Link")
                                }
                                else {
                                    browser.back();
                                    browser.pause(data.averagepause);
                                }
                            })
                        });
                    }
                    else {
                            console.log("Current Coverage Link is not present in Health Menu")
                            browser.back();
                            browser.pause(data.averagepause);
                        }
                  });
            }
            else {
                console.log("Health Tab is not present in header")
            }
        });
    });

};


