var seleniumServer = require('selenium-server');
var chromedriver=require('chromedriver');
var ieDriver = require('iedriver');
var firefoxDriver = require('geckodriver');
var data = require('./test_data/IBC_TestData.js');
sauce_labs_username = "Nishant-Kolte";
sauce_labs_access_key = "82105826-03a4-4a4b-a195-f761fcc8ae4e";


require('nightwatch-cucumber')({
    supportFiles: ['./utils/TestExecListener.js'],
    stepTimeout: 300000,
    defaultTimeoutInterval: 300000,
    nightwatchClientAsParameter: true
});

module.exports = {
    output_folder: 'reports',
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: "objects",
    live_output: false,
    disable_colors: false,
    test_workers: {
        enabled: false,
        workers: 3
    },
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: 5555,
        platform: 'Windows 10',
        cli_args: {
            'webdriver.chrome.driver' :'C:/Users/Nishant-Kolte/Downloads/chromedriver.exe',
            'webdriver.ie.driver': 'C:/Users/Nishant-Kolte/Downloads/new/IEDriverServer.exe',
            'webdriver.firefox.driver': firefoxDriver.path
        }
    },
    test_settings: {
        default: {
            launch_url: 'http://localhost',
            page_objects_path: 'objects',
            selenium_host: '127.0.0.1',
            selenium_port: 5555,
            silent: true,
            disable_colors: false,
            screenshots: {
                enabled: true,
                on_failure: true,
                on_error: true,
                path: 'screenshots'
            },
            desiredCapabilities: {
                browserName: data.NameofThebrowser,
                // browserName: "internet explorer",
                javascriptEnabled: true,
                acceptSslCerts: true
            },
        },

        SauceLabs: {
            launch_url: 'http://ondemand.saucelabs.com:80',
            selenium_port: 80,
            selenium_host: 'ondemand.saucelabs.com',
            silent: true,
            username: sauce_labs_username,
            access_key: sauce_labs_access_key,
            screenshots: {
                enabled: false,
                path: '',
            },
            globals: {
                waitForConditionTimeout: 10000,
            },
            desiredCapabilities: {
                name:'IBC Test',
                browserName: 'chrome',                   //browser to be used
                platform: 'Windows 10',                         //OS to be used
                version: '46',                                  //browser version to be used
                parentTunnel:'erik-horrell',
                tunnelIdentifier:'MercerTunnel2',
            }
        },
        microsoftedge: {
            desiredCapabilities: {
                browserName: 'microsoftedge'
            }
        },
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts : true
                }
        },
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ie: {
            desiredCapabilities: {
                browserName: 'internet explorer',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        // Appium Config details
        iphone : {
            launch_url : "http://" + sauce_labs_username + ":" + sauce_labs_access_key +
            "@ondemand.saucelabs.com:80/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "iphone",
                platformName: "iOS",
                deviceName: "iPhone Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPHONE EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ipad : {
            launch_url : "http://127.0.0.1:4723/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "ipad",
                platformName: "iOS",
                deviceName: "iPad Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPAD EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        android : {
            launch_url : "http://localhost:5554/wd/hub",
            selenium_port  : 5554,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "android",
                platformName: "ANDROID",
                deviceName: "",
                version: "",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        }

    }

}

