module.exports = {
    sections: {
        MBCLoginPage: {
            selector: 'body',
            elements: {
                labelLoginTextMessage: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Log in to your existing account.")]'
                },
                inputusername: {locateStrategy: 'xpath', selector: '//*[@id="usernameId"]'},
                inputpassword: {locateStrategy: 'xpath', selector: '//*[@id="passwordId"]'},

                btnSubmit: {locateStrategy: 'xpath', selector: '//input[@value = "Submit"]'},
                labelalreadyloginTextMessage: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Register your account now.")]'
                },
                btnGetStarted: {locateStrategy: 'xpath', selector: '//input[@value="Get Started"]'},
                linkforgotpassword: {selector: '.inline_links  a:nth-child(2)'},
                linkPrivacyPolicy: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Privacy Policy")]'},
                //               linkTermsofUse : {locateStrategy: 'xpath',
                //                   selector: '//a[contains(text(),"Terms of Use")]'
                //               },
                //               linkTermsofUse : {locateStrategy: 'xpath',
                //                   selector: '//a[contains(text(),"Terms of Use")]'
                //               },
                linkTermsofUse: {selector: 'div.footercontrol_links:nth-child(2) > ul:nth-child(1) > li:nth-child(3)'},
                LogoutMessage: {
                    locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"You have now logged out. We recommend that you close your Web browser to protect your personal information.")]'
                }
            }
        },

        MBCHeaderlinks: {
            selector: 'body',
            elements: {

                dashboard_EditProfileLink: {
                    locateStrategy: 'xpath',
                    selector: '//span[text()="Edit profile"]'

                },
                linkLogOut: {
                    locateStrategy: 'xpath',
                    selector: '//a[text()=\'Logout\']'
                }
            }
        }

    }
};
