module.exports = {
    sections: {
        IBCLoginPage: {
            selector: 'body',
            elements: {
                labelLoginTextMessage: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Log in to your existing account.")]'
                },
                inputusername: {locateStrategy: 'xpath', selector: '//*[@id="usernameId"]'},
                inputpassword: {locateStrategy: 'xpath', selector: '//*[@id="passwordId"]'},

                btnSubmit: {locateStrategy: 'xpath', selector: '//input[@value = "Submit »"]'},
                labelalreadyloginTextMessage: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Register your account now.")]'},
                btnGetStarted: {locateStrategy: 'xpath', selector: '//input[@value="Get Started »"]'},
                linkforgotpassword: {selector: '.inline_links  a:nth-child(2)'},
                linkPrivacyPolicy: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Privacy Policy")]'},
                //               linkTermsofUse : {locateStrategy: 'xpath',
                //                   selector: '//a[contains(text(),"Terms of Use")]'
                //               },
                //               linkTermsofUse : {locateStrategy: 'xpath',
                //                   selector: '//a[contains(text(),"Terms of Use")]'
                //               },
                linkTermsofUse: {selector: 'div.footercontrol_links:nth-child(2) > ul:nth-child(1) > li:nth-child(3)'},
                LogoutMessage: {
                    locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"You have now logged out. We recommend that you close your Web browser to protect your personal information.")]'
                }
            }
        },

        IBCHeaderlinks: {
            selector: 'body',
            elements: {
                linkContactUs: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Contact Us")]'
                },
                linkLoginManagement: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Login Management")]'
                },
                linkHelp: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Help")]'
                },
                linkSiteMap: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Site Map")]'
                },
                linkContactUsinheader: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Contact Us")]'
                },
                linkLogOut: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Log Out")]'
                }
            }
        },
        IBCMenu: {
            selector: 'body',
            elements: {
                HeaderTabs: {
                    locateStrategy: 'xpath',
                    selector: '//ul[@class="tabnavbar_navigator"]/li/a'
                },

                PlansLink: {
                    locateStrategy: 'xpath',
                    selector: '//ul[@class="tabnavbar_navigator_level2_selected"]/li/a'
                },

                HomeTab: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Home")]'
                },
                WealthTab: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Wealth')]"
                },
                Plan401: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator_level3']/li/a//span[contains(text(),'401')]"
                },
                PlanMCNNonRepresented: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator_level3']/li/a//span[contains(text(),'MCN Traditional - Non-Represented')]"
                },
                PlansDropdown: {
                    locateStrategy: 'xpath',
                      selector: "//span[@class='tabnavbar_navigator_level2_link_items']"
                     // selector: "//span[contains(text(),'Plans')]"
                },

                Plans: {
                    locateStrategy: 'xpath',
                    selector: "//span[contains(text(),'Plans')]/../../ul/li//a"
                },


                HealthTab: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Health')]"
                },
                AbsenceTab: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator']/li/a/span[contains(text(),'Absence')]"
                },
                TotalRewardsTab: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Total Rewards')]"
                },
                FormsTab: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Forms')]"
                },
                ResourceCenterTab: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Resource Center')]"
                },
                AdministrationTab: {
                    locateStrategy: 'xpath',
                    selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Administration')]"
                },
                ImagemyWealthContainer: {
                    locateStrategy: 'xpath',
                    selector: '//h2[contains(text(),"myWealth")]'
                },
                ImagemyHealthContainer: {
                    locateStrategy: 'xpath',
                    selector: '//h2[contains(text(),"myHealth")]'
                },
                ImagemyAbsenceContaier: {
                    locateStrategy: 'xpath',
                    selector: '//h2[contains(text(),"myAbsence")]'
                },
            }
        },
        IBCHealthPage: {
            selector: 'body',
            elements: {
                linkOverview: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Overview")][1]'
                },
                linkBeneficiaries: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Beneficiaries")]'
                },
                linkCurrentCoverages: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Current Coverages")]'
                },
                linkFamilydata: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Family Data")]'
                },
                linkLifeStatusChange: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Life Status Change")]'
                },
                linkProviders: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Providers")]'
                },
                linkTools: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Tools")][0]'

                },
                linkForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Forms")][1]'
                },
                WhattoIhavecontainer: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"What do I have? Mercer Testing")]'
                },
                WhattoIneedcontainer: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"What do I need?")]'
                },
            }
        },
        IBCWealthPage: {
            selector: 'body',
            elements: {
                linkOverview: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Overview")][0]'
                },
                ListPlans: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Plans")][0]'
                },
                linkResources: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Resources")][0]'
                },
                linkForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Forms")][0]'
                },
            }
        },
        IBCFormsPage: {
            selector: 'body',
            elements: {
                tabWealthForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Wealth Forms")]'
                },
                tabHealthForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Health Forms")]'
                },

            }
        },
        IBCLoginManagementPage: {
            selector: 'body',
            elements: {
                labelUsername: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"User Name")]'
                },
                labelPassword: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Password")]'
                },
                labelEmail: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"E-mail")]'
                },
                labelSecurityQuestions: {
                    locateStrategy: 'xpath',
                    selector: '//p[contains(text(),"Security Questions")]'
                },
                labelAnswers: {
                    locateStrategy: 'xpath',
                    selector: '//p[contains(text(),"Answers")]'
                },
                linkChangeUsername: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your User Name")]'
                },
                linkChangePassword: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your Password")]'
                },
                linkChangeEmailaddress: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your E-mail Address")]'
                },
                linkChangeSecurityQuestions: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your Security Questions")]'
                }
            }
        },
        IBCSecurityQuestionsPage: {
            selector: 'body',
            elements: {
                labelUsername: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"User Name")]'
                },
                labelPassword: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Password")]'
                },
                labelEmail: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"E-mail")]'
                },
                labelSecurityQuestions: {
                    locateStrategy: 'xpath',
                    selector: '//p[contains(text(),"Security Questions")]'
                },
                labelAnswers: {
                    locateStrategy: 'xpath',
                    selector: '//p[contains(text(),"Answers")]'
                },
                linkChangeUsername: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your User Name")]'
                },
                linkChangePassword: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your Password)]'
                },
                linkChangeEmailaddress: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your E-mail Address")]'
                },
                linkChangeSecurityQuestions: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Change your Security Questions")]'
                },
                Question1ListItems: {
                    locateStrategy: 'xpath',
                    selector: '//select[contains(@id,"QUESTION__0")]'
                },
                Question2ListItems: {
                    locateStrategy: 'xpath',
                    selector: '//select[contains(@id,"QUESTION__1")]'
                },
                Question3ListItems: {
                    locateStrategy: 'xpath',
                    selector: '//select[contains(@id,"QUESTION__2")]'
                },
                inputtxtfieldAnswer1: {
                    locateStrategy: 'xpath',
                    selector: '//input[contains(@id,"ANSWER__0")]'
                },
                inputtxtfieldAnswer2: {
                    locateStrategy: 'xpath',
                    selector: '//input[contains(@id,"ANSWER__2")]'
                },
                inputtxtfieldAnswer3: {
                    locateStrategy: 'xpath',
                    selector: '//input[contains(@id,"ANSWER__3")]'
                },
                btnSave: {
                    locateStrategy: 'xpath',
                    selector: '//input[@type="submit"]'
                },
                confirmationmsg: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"You have successfully updated your security settings.")]'
                }
            }
        },
        IBCContactUsPage: {
            selector: 'body',
            elements: {
                ChatContainer: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Chat online with a Service Representative")]'
                },
                linkChatOnline: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Chat online with a Service Representative")]'
                },

                EmailUsContainer: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Visit the Message Center")]'
                },

                linkVisitMessageCenter: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Visit the Message Center")]'
                },
            }
        },
        IBCSiteMapPage: {
            selector: 'body',
            elements: {
                HeadingSiteMap: {
                    locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Site Map")]'
                },
            }
        },
        IBCForgotPasswordfromLoginPage: {
            selector: 'body',
            elements: {
                labelSSN: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Last 4 Digits of Social Security Number:")]'
                },
                labelLastname: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Last Name:")]'
                },
                labelDOB: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Date of Birth (MM/DD/YYYY):")]'
                },
                labelPostalCode: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Postal code:")]'
                },
                inputSSN: {selector: "#TopazWebPartManager1_twp54834229_securityNumber"},
                inputLastname: {selector: "#TopazWebPartManager1_twp54834229_lastName"},
                inputDOB: {selector: "#TopazWebPartManager1_twp54834229_dob"},
                inputPostalCode: {selector: "#TopazWebPartManager1_twp54834229_zipCode"},
                btnNext: {selector: "#TopazWebPartManager1_twp54834229_nextButton"}
            }
        },
        IBCForgotPasswordYourSecurityQuestionsPage: {
            selector: 'body',
            elements: {
                labelYourSecurityQuestions: {selector: "#WebPart_twp56459656"},
                inputLastname: {selector: "#TopazWebPartManager1_twp54834229_lastName"},
                inputDOB: {selector: "#TopazWebPartManager1_twp54834229_dob"},
                inputPostalCode: {selector: "#TopazWebPartManager1_twp54834229_zipCode"},
                btnNext: {selector: "#TopazWebPartManager1_twp54834229_nextButton"},
                SecurityQuestion: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="field_one_left"]/div/div/p[1]/label'
                },
                inputlabelsecuriyquestion1: {selector: "p.form_label label"},
                inputSecurityAnswer1: {selector: "#TopazWebPartManager1_twp802737329_AnswerTextBox"},
                btnSQANext: {selector: "#TopazWebPartManager1_twp802737329_CaptureSQANext"},
                TitleSetupaccountinformation: {selector: "#WebPart_twp56459656"},
                // For Resetting Password from login Page
                inputnewpassword1: {selector: "#TopazWebPartManager1_tgwp1580586657_twp1580586657_MODIFYPASSWORD_NewPasswordTextBox"},
                inputreenternewpassword1: {selector: "#TopazWebPartManager1_tgwp1580586657_twp1580586657_MODIFYPASSWORD_ConfirmPasswordTextBox"},
                labelLoginInformation: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Login Information")][0]'
                },
                TitleLoginInformation: {selector: "TopazWebPartManager1_tgwp1580586657_twp1580586657_STEP_PERSONALINFO_TITLE"},
                labelCreateUserName: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Create User Name")]'
                },
                labelEnterYourUserName: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Enter your User Name")]'
                },
                labelCreatePassword: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Create Password")]'
                },
                labelEnterYourPassword: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Enter your new Password")]'
                },
                labelReEnterYourPassword: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Re-enter your new Password")]'
                },
                btnContainerIDNext: {selector: "#TopazWebPartManager1_tgwp1580586657_twp1580586657_StartNavigationTemplateContainerID_NextButton"},
                TitleReviewAndSaveInformation: {selector: "#TopazWebPartManager1_tgwp1580586657_twp1580586657_STEP_REVIEW_TITLE"},
                TitleReviewDescription: {selector: "#TopazWebPartManager1_tgwp1580586657_twp1580586657_STEP_REVIEW_DESCRIPTION"},
                btnFinishNext: {selector: "#TopazWebPartManager1_tgwp1580586657_twp1580586657_FinishNavigationTemplateContainerID_NextButton"},
                TitleInformationsaved: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp56459656"]/tbody/tr/td/h1'
                },
                SubTitleInformationsaved: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp1273258773"]/tbody/tr/td/div/div/p/text()'
                },
                btnContinue: {
                    locateStrategy: 'xpath',
                    selector: '//*[@type="submit"]'
                },
                // For Resetting Password from home page
                inputOldPassword: {selector: "#TopazWebPartManager1_twp1826874299_OldPasswordTextBox"},
                inputNewPassword: {selector: "#TopazWebPartManager1_twp1826874299_NewPasswordTextBox"},
                inputReNewPassword: {selector: "#TopazWebPartManager1_twp1826874299_ConfirmPasswordTextBox"},
                btnSaveModifyPassword: {selector: "#TopazWebPartManager1_twp1826874299_ModifyPasswordNext"},
                TitleConfirmationmessage: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"You have successfully updated your account login information.")]'
                }
            }
        },
        IBCAdminLandingPage: {
            selector: 'body',
            elements: {
                linkEmployeesearch: {
                    locateStrategy: 'xpath',
                    selector: '//table/tbody/tr/td/div/div[1]/div/a/span'
                    //*[@id="WebPart_twp1268416957"]/tbody/tr/td/div/div[1]/div/a/span
                },
                SearchBySSN: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="searchCriteriaCell"]/select/option[2]'
                },
                SearchByLastName: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="searchCriteriaCell"]/select/option[1]'
                },

                inputKeyword: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="searchTextCell"]/input'
                },


                btnSearchProxy: {
                    locateStrategy: 'xpath',
                    selector: '//input[@type="submit"]'
                },
                selectlink: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="TableCellEmployee"]/a'
                },

                SSNfirstrowresult: {
                    locateStrategy: 'xpath',
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[2]/td[2]'
                },
                LastNamefirstrowresult: {
                    locateStrategy: 'xpath',
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[2]/td[3]'
                },
                FirstNamefirstrowresult: {
                    locateStrategy: 'xpath',
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[2]/td[4]'
                },

                labelImpersonate: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"You are currently impersonating")]'
                },

                linkEndImpersonation: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"End Impersonation")]'
                }
            },
        }
    }
};
