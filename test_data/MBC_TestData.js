module.exports = {
    TestingEnvironment: 'QAI',
  // TestingEnvironment: 'CITEST',
   // TestingEnvironment: 'CSO',
   // TestingEnvironment: 'PROD',

    shortpause: 2000,
    averagepause: 7000,
    longpause: 30000,
    shortwaittime: 20000,
    averagewaittime: 40000,
    longwaittime: 120000,

    urlQAI: 'https://auth-qai.mercerbenefitscentral.com/MBCQA3/login.tpz',
    urlCITEST: 'https://auth-cit.mercerbenefitscentral.com/MBCTRA/Login.tpz',
    urlCSO: 'https://auth-cso.mercerbenefitscentral.com/MBCTRA/Login.tpz',
    urlPROD: 'https://auth.mercerbenefitscentral.com/MBCTRA/Login.tpz',



    usersQAI: {
        MBCUSER: {
          username: 'mbcqa32600', password: 'Test0001' },

    },
    usersCITEST: {
        MBCUSER: {
            username: 'MBCTRA0005', password: 'MBCTRApass1' },
    },
    usersCSO: {
        MBCUSER: {
            username: 'MBCTRA0007', password: 'MBCTRApass1' },
    },
    usersPROD: {
        MBCUSER: {
            username: 'MBCTRA0002', password: 'MBCTRAPASS1' },
    },

    // QAI Environment - Test data for Resetting Password

    // SSN : "4988",
    // LastName : "LNAME4988",
    // DOB : '10/12/1963',
    // PostalCode : '02062',
    // NewPassword: 'test0002',
    // DefaultPassword: 'test0001',
    //


};
