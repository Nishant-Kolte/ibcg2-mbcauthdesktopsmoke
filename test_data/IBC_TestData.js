module.exports = {
    TestingEnvironment: 'QAI',
  // TestingEnvironment: 'CITEST',
   // TestingEnvironment: 'CSO',
   // TestingEnvironment: 'PROD',
   //  TestingEnvironment: 'CIDEV',

     NameofThebrowser: "chrome",
  // NameofThebrowser: "internet explorer",

    shortpause: 2000,
    averagepause: 7000,
    longpause: 30000,
    shortwaittime: 20000,
    averagewaittime: 40000,
    longwaittime: 120000,

   urlQAI: 'https://qai.ibenefitcenter.com/QAWORK/login.tpz',
   urlCITEST: 'https://ctesti.ibenefitcenter.com/SLSDM/login.tpz',
    urlCSO: 'https://csoi.ibenefitcenter.com/SLSDM/login.tpz',
    urlPROD: 'https://ibenefitcenter2.mercerhrs.com/SLSDM/login.tpz',
    urlCIDEV: 'https://cdevi.ibenefitcenter.com/login.tpz',


    usersQAI: {
        IBCUSER: {
          username: 'QAWORK0025', password: 'QAWORKpass1' },// QAWORK1903/QAWORKpass1 - DTE and 401
         //   username: 'test501001', password: 'TEST0001',
    },
    usersCITEST: {
        IBCUSER: {
            username: 'testqa1201', password: 'test0001' },
    },
    usersCSO: {
        IBCUSER: {
            username: 'test1001', password: 'Test0001' }, //testqa1003/test0001
    },
    usersPROD: {
        IBCUSER: {
            username: 'testqa1002', password: 'test0001' },
    },
    usersCIDEV: {
        IBCUSER: {
            username: 'testqa1002', password: 'Test0001' },
    },

    // QAI Environment - Test data for Resetting Password
    SSN : "4988",
    LastName : "LNAME4988",
    DOB : '10/12/1963',
    PostalCode : '02062',
    NewPassword: 'test0002',
    DefaultPassword: 'test0001',

    SSNforProxyCIDEV: "200001201",  // CIDEV Environment
    SSNforProxyQAI: "555000016",  // QAI Environment - Client - PBSPHL 892670507
    SSNforProxyCITEST: "200001201",  // CSO Environment
    SSNforProxyCSO: "200001201",  // CSO Environment
    SSNforProxyPROD: "200001201",  // PROD Environment

    //DCPlan: "401",   // DC Plan
    DBPlan: "MCN"   // DB Plan


};
